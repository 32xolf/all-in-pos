﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using All.In.POS.API.Helper;

namespace All_InPOS
{
    public partial class APos : Form
    {
        public APos()
        {
            Thread t = new Thread(new ThreadStart(startSplash));
            t.Start();
            Thread.Sleep(5000);
            APosHelper.Instance.IsDirExist();
            Thread.Sleep(5000);
            APosHelper.Instance.IsDBExist();
            Thread.Sleep(5000);
            InitializeComponent();
            t.Abort();
        }

        void startSplash()
        {
            Application.Run(new SplashScreen());
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnshut_click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
