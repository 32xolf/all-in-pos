﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using All.In.POS.API.Logging.SlfLog4Net;
using slf4net;



namespace All_InPOS
{
    public partial class MainDashboard : Form
    {
        private static readonly ILogger _logger = LoggerFactory.GetLogger(typeof(MainDashboard));

        public MainDashboard()
        {
            InitializeComponent();
            SidePanel.Height = btnDashboard.Height;
            SidePanel.Top = btnDashboard.Top;
            btnDashboard.ForeColor = Color.FromArgb(98, 113, 123);
            btnProduct.ForeColor = Color.White;
            btnInventory.ForeColor = Color.White;
            btnSales.ForeColor = Color.White;
            btnPOS.ForeColor = Color.White;

            SlfLogginFacade.InitLogging();

            _logger.Info("test1");            
          
        }

        private void btnDashboard_Click(object sender, EventArgs e)
        {
            SidePanel.Height = btnDashboard.Height;
            SidePanel.Top = btnDashboard.Top;
            btnDashboard.ForeColor = Color.FromArgb(98, 113, 123);
            btnProduct.ForeColor = Color.White;
            btnInventory.ForeColor = Color.White;
            btnSales.ForeColor = Color.White;
            btnPOS.ForeColor = Color.White;
          
        }

        private void btnProduct_Click(object sender, EventArgs e)
        {
            SidePanel.Height = btnProduct.Height;
            SidePanel.Top = btnProduct.Top;
            btnDashboard.ForeColor = Color.White;
            btnInventory.ForeColor = Color.White;
            btnSales.ForeColor = Color.White;
            btnPOS.ForeColor = Color.White;
            btnProduct.ForeColor = Color.FromArgb(98, 113, 123);
          
        }

        private void btnInventory_Click(object sender, EventArgs e)
        {
            SidePanel.Height = btnInventory.Height;
            SidePanel.Top = btnInventory.Top;
            btnDashboard.ForeColor = Color.White;
            btnProduct.ForeColor = Color.White;
            btnSales.ForeColor = Color.White;
            btnPOS.ForeColor = Color.White;
            btnInventory.ForeColor = Color.FromArgb(98, 113, 123);
           
        }

        
        private void btnSales_Click(object sender, EventArgs e)
        {
            SidePanel.Height = btnSales.Height;
            SidePanel.Top = btnSales.Top;
            btnDashboard.ForeColor = Color.White;
            btnProduct.ForeColor = Color.White;
            btnInventory.ForeColor = Color.White;
            btnPOS.ForeColor = Color.White;
            btnSales.ForeColor = Color.FromArgb(98, 113, 123);
            
        }

        private void btnPOS_Click(object sender, EventArgs e)
        {
            SidePanel.Height = btnPOS.Height;
            SidePanel.Top = btnPOS.Top;
            btnDashboard.ForeColor = Color.White;
            btnProduct.ForeColor = Color.White;
            btnInventory.ForeColor = Color.White;
            btnSales.ForeColor = Color.White;
            btnPOS.ForeColor = Color.FromArgb(98, 113, 123);
           
        }
    }
}
