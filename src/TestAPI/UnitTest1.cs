﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using All.In.POS.API.Util;
using All.In.POS.API.Logging;
using All.In.POS.API.Logging.SlfLog4Net;
using slf4net;

namespace TestAPI
{
    [TestClass]
    public class UnitTest1
    {
        private static readonly ILogger _logger = LoggerFactory.GetLogger(typeof(UnitTest1));
        [TestMethod]
        public void TestMethod1()
        {

            SlfLogginFacade.InitLogging();
            _logger.Info("Unit Test");

        }
    }
}
