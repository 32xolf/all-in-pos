﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using slf4net;
using log4net.Config;

namespace All.In.POS.API.Logging.SlfLog4Net
{
   
   public static class SlfLogginFacade 
    {

      
        public static void InitLogging()
        {
            // Create log4net ILoggerFactory and set the resolver
            var factory = new slf4net.log4net.Log4netLoggerFactory();
            var resolver = new SimpleFactoryResolver(factory);
            LoggerFactory.SetFactoryResolver(resolver);
            Util.UtilService _util = new Util.UtilService();
        }

     
    }
}
