﻿using slf4net;

namespace All.In.POS.API.Logging.SlfLog4Net
{
    /// <summary>
    /// A simple implementation of IFactoryResolver
    /// </summary>
    public class SimpleFactoryResolver : IFactoryResolver
    {
        private ILoggerFactory _factory;

        /// <summary>
        /// The constructor takes the ILoggerFactory to be used by slf4net
        /// </summary>
        public SimpleFactoryResolver(ILoggerFactory factory)
        {
            _factory = factory;
        }

        /// <summary>
        /// IFactoryResolver GetFactory() implementation
        /// </summary>
        public ILoggerFactory GetFactory()
        {
            return _factory;
        }
    }
}
