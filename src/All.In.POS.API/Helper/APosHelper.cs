﻿using All.In.POS.API.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using slf4net;
using System.IO;


namespace All.In.POS.API.Helper
{
    /// <summary>
    /// Author: Flote Fuertes
    /// </summary>
    public class APosHelper : IHelperSingletonContainer
    {

        private static readonly ILogger _logger = LoggerFactory.GetLogger(typeof(APosHelper));
        private APosHelper()
        {

            if (!IsDirExist())
            {
                InitDir();
            }

            if (!IsDBExist())
            {
                InitDB();
            }
        }
        private void InitDir()
        {
            if (!Directory.Exists(DirPaths.DataFolder))
                Directory.CreateDirectory(DirPaths.DataFolder);
            if (!Directory.Exists(DirPaths.UploadFolder))
                Directory.CreateDirectory(DirPaths.UploadFolder);
            if (!Directory.Exists(DirPaths.UploadedFolder))
                Directory.CreateDirectory(DirPaths.UploadedFolder);
            if (!Directory.Exists(DirPaths.ToolsFolder))
                Directory.CreateDirectory(DirPaths.ToolsFolder);
            if (!Directory.Exists(DirPaths.DBFolder))
                Directory.CreateDirectory(DirPaths.DBFolder);
            if (!Directory.Exists(DirPaths.BackUpFolder))
                Directory.CreateDirectory(DirPaths.BackUpFolder);
            _logger.Info("Create Dir");
        }

        private void InitDB()
        {
            var _dir = Path.Combine(DirPaths.DBFolder, FileName.DBFile);
            var _path1 = Path.Combine(DirPaths.BackUpFolder,FileName.DBFile);
           

            if (!File.Exists(_dir))
            {
                FileInfo f1 = new FileInfo(_path1);
                f1.CopyTo(_dir);
            }
                _logger.Info("Create Db");
        }
        /// <summary>
        /// Checks if all directory Exists
        /// </summary>
        /// <returns>boolean type</returns>
        public bool IsDirExist()
        {
            int count = 0;


            string _dir = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

            List<string> Dirs = new List<string>(Directory.EnumerateDirectories(_dir));

            foreach (var dir in Dirs)
            {
                var _dirs = dir.Substring(dir.LastIndexOf(Path.DirectorySeparatorChar) + 1);

                if (_dirs == DirPaths.DataFolder || _dirs == DirPaths.UploadFolder
                   || _dirs == DirPaths.UploadedFolder || _dirs == DirPaths.ToolsFolder)
                {
                    count++;
                }
            }

            if (count == 6)
                return true;

            return false;
        }
        /// <summary>
        /// Checks if DB Exists
        /// </summary>
        /// <returns></returns>
        public bool IsDBExist()
        {
            var _dir = Path.Combine(DirPaths.DBFolder, FileName.DBFile);
            if (File.Exists(_dir))
                return true;

            return false;
        }

        private static Lazy<APosHelper> instance = new Lazy<APosHelper>(() => new APosHelper());
        public static APosHelper Instance = instance.Value;

    }
}
