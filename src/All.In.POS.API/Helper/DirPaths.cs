﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace All.In.POS.API.Helper
{
    public static class DirPaths
    {
        public const string DataFolder = "Data";
        public const string UploadFolder = "Upload";
        public const string UploadedFolder = "Uploaded";
        public const string DBFolder = "ALS";
        public const string ToolsFolder = "Tools";
        public const string BackUpFolder = "Backup";


    }

    public static class FileName
    {
        public const string DBFile = "allinpos.db";
    }
}
