﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace All.In.POS.API.Interface
{
   public interface IHelperSingletonContainer
   {

        bool IsDirExist();
        bool IsDBExist();
   }
}
