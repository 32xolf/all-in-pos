﻿using System.IO;
using System.Reflection;
using log4net.Config;
using slf4net;

namespace All.In.POS.API.Util
{
    public static class Util 
    {

    
         public static ILogger  GetLog( string log,string txt)
        {
          ILogger _log = slf4net.LoggerFactory.GetLogger(log);
            return _log;
        }

       
        
    }

    public class UtilService
    {
        public UtilService()
        {
            Assembly assem = Assembly.GetAssembly(GetType());
            string DirAssem = Path.GetDirectoryName(assem.Location);
            initialise(DirAssem + "\\log4net.config");
 
        }

        void initialise(string pathname)
        {
            FileInfo _file = new FileInfo(pathname);
            log4net.Config.XmlConfigurator.Configure(_file);
        }

        void InitDir()
        {
            
        }
    }
}
